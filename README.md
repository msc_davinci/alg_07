Programa Java para resolver el problema de los 8 rompecabezas

### Compilar y ejecutar
``` 
$ javac Puzzle.java
$ java Puzzle
```
 
### Referencia
http://www.geeksforgeeks.org/branch-bound-set-3-8-puzzle-problem/
